from .models import Result
from rest_framework import viewsets
from rest_framework import permissions
from sensor_api.results_api.serializers import ResultSerializer


class ResultViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Result.objects.all().order_by('-id')
    serializer_class = ResultSerializer
    # permission_classes = [permissions.IsAuthenticated]