from django.apps import AppConfig


class ResultsApiConfig(AppConfig):
    name = 'results_api'
