from django.db import models

class Result(models.Model):
    id = models.AutoField(primary_key=True)
    temperature = models.CharField(max_length=10)
    humidity = models.CharField(max_length=10)

    class Meta:
        verbose_name_plural = "Results"

    
    def __str__(self):
        return f"Result nr: {self.id}"
